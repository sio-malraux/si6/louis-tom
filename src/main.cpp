#include<cstring>
#include <libpq-fe.h>
#include <iostream>
#include <iomanip>
using namespace std;
// std::setw 
#include <stdio.h> 
#include <string.h>
int main()
{
  PGPing Ping;
  std::string conninfo = "port=5432";
  std::string user = "t.fleuret";
  std::string dbname = "t.fleuret";
  std::string host = "postgresql.bts-malraux72.net";
  conninfo += " host=" + host;
  Ping = PQping(conninfo.c_str());

  if(Ping == PQPING_OK)
  {
    std::cout << "La connexion au serveur base de donnée à été établie avec succès" << std::endl;
    PGconn *connexion;
    conninfo += " user=" + user + " dbname=" + dbname;
    connexion = PQconnectdb(conninfo.c_str());
    int nFields, i, j;
    PGresult *resultat;
    ExecStatusType status_return;
    ConnStatusType Status;
    Status = PQstatus(connexion);

    // const char *param="encodage";
    if(Status == CONNECTION_OK)
    {
      std::cout << "La connexion au serveur de base de données '" << PQhost(connexion) << "' a été établie avec les paramètres suivants : " << std::endl;
      std::cout << " * utilisateur : " <<
      PQuser(connexion) << std::endl;
      std::cout << " * mot de passe : " ;

      for(int i = 0; i < sizeof(PQpass(connexion)) ; i++)
      {
        std::cout << "*";
      }

      std::cout << std::endl; // Fin d'affichage du mot de passe
      std::cout << " * base de données : " << PQhost(connexion) << std::endl; std::cout << " * port TCP : " << PQport(connexion) << std::endl; std::cout << " * chiffrement SSL : ";

      if(PQsslInUse(connexion) == 1) {
      std::cout << "true";
    }
    else
    {
      std::cout << "false";
    }

    std::cout << std::endl; // Fin d'affichage de l'usage du SSL
    std::cout << " * encodage : " << PQparameterStatus(connexion, "server_encoding") << std::endl; 
    std::cout << " * version du protocole : " << PQprotocolVersion(connexion) << std::endl;
    std::cout << " * version du serveur : " << PQserverVersion(connexion) << std::endl;
    std::cout << " * version de la bibliothèque libpq du client : " << PQlibVersion() << std::endl;
    resultat = PQexec(connexion, "SELECT \"animal\".\"id\",\"animal\".\"nom\" AS \"nom de l'animal\",\"animal\".\"sexe\",\"animal\".\"date_naissance\" AS \"date de naissance\",\"animal\".\"commentaires\",\"Race\".\"nom\" AS \"race\",\"Race\".\"description\" FROM \"si6\".\"animal\" INNER JOIN \"Race\" ON \"animal\".\"race_id\" = \"Race\".\"id\" WHERE \"Race\".\"nom\" = 'Singapura' AND \"sexe\" = 'Femelle'");
    status_return = PQresultStatus(resultat);

    if(status_return == PGRES_EMPTY_QUERY)
    {
      std::cerr << "La chaîne envoyée au serveur était vide." << std::endl;
    }
    else if(status_return == PGRES_COMMAND_OK)
    {
      std::cerr << "Fin avec succès d'une commande ne renvoyant aucune donnée." << std::endl;
    }
    else if(status_return == PGRES_TUPLES_OK)
    {
      int nb_lignes = PQntuples(resultat);
      int nb_colonnes = PQnfields(resultat);
      int compteur = 0;
      int maximum = 0;
      std::string separation = " | ";

      // Calculer la largeur max d'un nom de champ
      for(int i = 0; i < nb_colonnes; ++i)
      {
        int longueur = strlen(PQfname(resultat, i));

        if(longueur > maximum)
        {
          maximum = longueur;
        }
      }
       for(int e = 0; e < nb_colonnes * maximum + (nb_colonnes * 3 + 2); e++)
       {
       std::cout << "-";
       }
       std::cout << std::endl;

      // Afficher l'en-tete pour chaque champ, afficher le nom du champ
      for(int i = 0; i < nb_colonnes; ++i)
      {
        std::cout << separation << std::setw(maximum) << std::left << PQfname(resultat, i);
      }
        std::cout << separation;
	std::cout << std::endl;
       for(int e = 0; e < nb_colonnes * maximum + (nb_colonnes * 3 + 2); e++)
       {
       std::cout << "-";
       }
       std::cout << std::endl;
      // Afficher les tuples Pour chaque tuple afficher la ligne des valeurs Pour chaque colonne, afficher la valeur de la colonne
      for(int i = 0; i < nb_lignes; i++)
      {
        for(int j = 0; j < nb_colonnes; j++)
        {

	int max = std::strlen(PQgetvalue(resultat,i,j));
	char *coucou = PQgetvalue(resultat,i,j);
			if(maximum < max)
			{
			coucou[maximum] = '\0';

				for(int o = maximum-3 ; o < maximum; o++)
				{
				coucou[o] = '.';
				}
				}
          std::cout << separation << std::setw(maximum) << std::left << PQgetvalue(resultat, i, j) ;
        }
	std::cout << separation;
        std::cout << std::endl;
      }
       for(int e = 0; e < nb_colonnes * maximum + (nb_colonnes * 3 + 2); e++)
       {
       std::cout << "-";
       }

      std::cout << std::endl;
      std::cout << "L'exécution de la requête SQL a retourné " << nb_lignes << " enregistrements" << std::endl;
    }
    else if(status_return == PGRES_COPY_OUT)
    {
      std::cerr << "Début de l'envoi (à partir du serveur) d'un flux de données." << std::endl;
    }
    else if(status_return == PGRES_COPY_IN)
    {
      std::cerr << "Début de la réception (sur le serveur) d'un flux de données." << std::endl;
    }
    else if(status_return == PGRES_BAD_RESPONSE)
    {
      std::cerr << "La réponse du serveur n'a pas été comprise." << std::endl;
    }
    else if(status_return == PGRES_NONFATAL_ERROR)
    {
      std::cerr << "Une erreur non fatale (une note ou un avertissement) est survenue." << std::endl;
    }
    else if(status_return == PGRES_FATAL_ERROR)
    {
      std::cerr << "Une erreur fatale est survenue." << std::endl;
    }
    else if(status_return == PGRES_COPY_BOTH)
    {
      std::cerr << "Lancement du transfert de données Copy In/Out (vers et à partir du serveur). Cette fonctionnalité est seulement utilisée par la réplication en flux, ce statut ne devrait donc pas apparaître dans les applications ordinaires." << std::endl;
    }
    else if(status_return == PGRES_SINGLE_TUPLE)
    {
      std::cerr << "La structure PGresult contient une seule ligne de résultat provenant de la commande courante. Ce statut n'intervient que lorsque le mode ligne-à-ligne a été sélectionné pour cette requête." << std::endl;
    }
  } // fin Status
} // fin Ping
return 0;
}

